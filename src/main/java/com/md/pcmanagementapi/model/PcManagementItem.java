package com.md.pcmanagementapi.model;

import com.md.pcmanagementapi.enums.PcCategory;
import com.md.pcmanagementapi.enums.PcStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementItem {
    private Long id;
    private Byte pcNumber;
    private String partName;
    private String modelName;
    private LocalDate purchaseDay;
    private String pcStatus;
    private Boolean isCheck;
    private LocalDate updateDay;
    private LocalDate checkDay;
}
