package com.md.pcmanagementapi.model;

import com.md.pcmanagementapi.enums.PcCategory;
import com.md.pcmanagementapi.enums.PcStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementResponse {
    private Long id;
    private Byte pcNumber;
    private String categoryName;
    private LocalDate purchaseDay;
    private String pcStatusName;
    private String isCheckName;
    private LocalDate updateDay;
    private LocalDate checkDay;
    private String note;
}
