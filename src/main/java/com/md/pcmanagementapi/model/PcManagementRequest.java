package com.md.pcmanagementapi.model;

import com.md.pcmanagementapi.enums.PcCategory;
import com.md.pcmanagementapi.enums.PcStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcManagementRequest {
    private Byte pcNumber;

    @Enumerated(value = EnumType.STRING)
    private PcCategory category;

    @Enumerated(value = EnumType.STRING)
    private PcStatus pcStatus;

    private String note;
}
