package com.md.pcmanagementapi.service;

import com.md.pcmanagementapi.entity.PcManagement;
import com.md.pcmanagementapi.model.PcManagementItem;
import com.md.pcmanagementapi.model.PcManagementRequest;
import com.md.pcmanagementapi.model.PcManagementResponse;
import com.md.pcmanagementapi.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement(PcManagementRequest request) {
        PcManagement addData = new PcManagement();
        addData.setPcNumber(request.getPcNumber());
        addData.setCategory(request.getCategory());
        addData.setPurchaseDay(LocalDate.now());
        addData.setPcStatus(request.getPcStatus());
        addData.setUpdateDay(LocalDate.now());
        addData.setCheckDay(LocalDate.now());
        addData.setNote(request.getNote());

        pcManagementRepository.save(addData);
    }

    public List<PcManagementItem> getPcManagements(){
        List<PcManagement> originlist = pcManagementRepository.findAll();

        List<PcManagementItem> result = new LinkedList<>();

        for (PcManagement pcManagement : originlist) {
            PcManagementItem addItem = new PcManagementItem();
            addItem.setId(pcManagement.getId());
            addItem.setPcNumber(pcManagement.getPcNumber());
            addItem.setPartName(pcManagement.getCategory().getPartName());
            addItem.setModelName(pcManagement.getCategory().getModelName());
            addItem.setPurchaseDay(pcManagement.getPurchaseDay());
            addItem.setPcStatus(pcManagement.getPcStatus().getName());
            addItem.setIsCheck(pcManagement.getPcStatus().getIsCheck());
            addItem.setUpdateDay(pcManagement.getUpdateDay());
            addItem.setCheckDay(pcManagement.getCheckDay());

            result.add(addItem);
        }

        return result;
    }

    public PcManagementResponse getPc(long id) {
        PcManagement originData = pcManagementRepository.findById(id).orElseThrow();

        PcManagementResponse response = new PcManagementResponse();
        response.setId(originData.getId());
        response.setPcNumber(originData.getPcNumber());
        response.setCategoryName(originData.getCategory().getPartName());
        response.setPurchaseDay(originData.getPurchaseDay());
        response.setPcStatusName(originData.getPcStatus().getName());

        response.setIsCheckName(originData.getPcStatus().getIsCheck() ? "예" : "아니요");

        response.setUpdateDay(originData.getUpdateDay());
        response.setCheckDay(originData.getCheckDay());
        response.setNote(originData.getNote());

        return response;
    }

}
