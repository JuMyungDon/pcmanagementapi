package com.md.pcmanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcStatus {
    WELL("양호", false),
    REQUIREDINSPECTION("점검 요망", true);

    private final String name;
    private final Boolean isCheck;
}
