package com.md.pcmanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcCategory {
    MONITOR("모니터", "32인치 4k VSG321UHD"),
    KEYBOARD("키보드", "레오폴드 FC750R"),
    MOUSE("마우스", "로지텍 G100"),
    DESKTOP("본체", "조립 pc");

    private final String partName;
    private final String modelName;
}
