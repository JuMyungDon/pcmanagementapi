package com.md.pcmanagementapi.entity;

import com.md.pcmanagementapi.enums.PcCategory;
import com.md.pcmanagementapi.enums.PcStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Byte pcNumber;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PcCategory category;

    @Column(nullable = false)
    private LocalDate purchaseDay;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PcStatus pcStatus;

    @Column(nullable = false)
    private LocalDate updateDay;

    @Column(nullable = false)
    private LocalDate checkDay;

    @Column(columnDefinition = "TEXT")
    private String note;
}
