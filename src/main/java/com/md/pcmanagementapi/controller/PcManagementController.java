package com.md.pcmanagementapi.controller;

import com.md.pcmanagementapi.model.PcManagementItem;
import com.md.pcmanagementapi.model.PcManagementRequest;
import com.md.pcmanagementapi.model.PcManagementResponse;
import com.md.pcmanagementapi.service.PcManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pc-management")
public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/pc")
    public String setPcManagement(@RequestBody PcManagementRequest request) {
        pcManagementService.setPcManagement(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<PcManagementItem> getPcManagements(){
        return pcManagementService.getPcManagements();
    }

    @GetMapping("/detail/{id}")
    public PcManagementResponse getPc(@PathVariable long id) {
        return pcManagementService.getPc(id);
    }
}
