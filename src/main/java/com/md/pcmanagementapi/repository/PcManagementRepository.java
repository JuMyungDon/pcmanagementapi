package com.md.pcmanagementapi.repository;

import com.md.pcmanagementapi.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository<PcManagement,Long> {
}
